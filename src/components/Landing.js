import React from 'react';
import Slider from "react-slick";
import Logo from '../assets/logo.png';
import Slide1 from '../assets/slide1.png';
import Slide2 from '../assets/slide2.png';
import Slide3 from '../assets/slide3.png';
import Slide4 from '../assets/slide4.png';
import Dealer1 from '../assets/dealer1.png';
import Dealer2 from '../assets/dealer2.png';
import Dealer3 from '../assets/dealer3.png';
import Dealer4 from '../assets/dealer4.png';
import Dealer5 from '../assets/dealer5.png';
import Dealer6 from '../assets/dealer6.png';
import Dealer7 from '../assets/dealer7.png';
import Dealer8 from '../assets/dealer8.png';
import Dealer9 from '../assets/dealer9.png';
import Dealer10 from '../assets/dealer10.png';
import Dealer11 from '../assets/dealer11.png';
import Dealer12 from '../assets/dealer12.png';
import Dealer13 from '../assets/dealer13.png';
import Dealer14 from '../assets/dealer14.png';
import Back from '../assets/back.svg';

const Landing = () => {
    const settings = {
        dots: false,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        speed: 1000,
        autoplaySpeed: 3000,
        centerMode: false,
        arrows: false,
    };

    return (
        <div className="h-screen w-screen">
            {/* HEADER */}
            <div className="flex items-center px-5 py-5 lg:py-10 z-10 w-full absolute">
                <div class="container mx-auto flex flex-row lg:pl-24">
                    <img src={Logo} alt="" className="w-1/4 max-w-max h-auto"></img>
                </div>
                <div class="container mx-auto flex flex-row-reverse lg:pr-24">
                    <h1 className="text-white md:text-2xl text-lg">CONTACT</h1>
                </div>
            </div>
            <div className="text-white">
                <Slider {...settings}>
                    <div className="relative">
                        <div className="flex items-center">
                            <img src={Slide1} alt="" className="w-screen h-auto"></img>
                            <div className="flex justify-center container absolute">
                                <h1 className="flex headertitle lg:text-6xl md:text-5xl text-2xl lg:max-w-3xl md:max-w-xl max-w-xs ">
                                    WE OWN LIVE DEALER
                                    STUDIOS AND BUILD THE
                                    BEST VIRTUAL GAMES.
                                </h1>
                            </div>
                        </div>
                    </div>
                    <div className="relative">
                        <div className="flex items-center">
                            <img src={Slide2} alt="" className="w-screen h-auto "></img>
                            <div className="flex-col container absolute">
                                <div className="flex justify-center">
                                    <h1 className="headertitle lg:text-6xl md:text-5xl text-2xl lg:max-w-3xl md:max-w-xl max-w-xs">
                                        OUR GAMES ARE LOCAL
                                    </h1>
                                </div>
                                <div className="flex justify-center ml-14 md:ml-12 lg:ml-28 mt-2 md:mt-8">
                                    <p className="headertitle2 lg:text-4xl md:text-2xl text-base lg:max-w-3xl md:max-w-xl max-w-xs">
                                        Customary games played by customers
                                        <be />
                                        of all generations in the markets
                                        <br />
                                        we build them for.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="relative">
                        <div className="flex items-center">
                            <img src={Slide3} alt="" className="w-screen h-auto"></img>
                            <div className="flex justify-center container absolute">
                                <h1 className="flex headertitle lg:text-6xl md:text-5xl text-2xl lg:max-w-3xl md:max-w-2xl max-w-xs">
                                    OUR GAMES ARE PLAYED
                                    <br />
                                    IN THE WORLDS LARGEST
                                    <br />
                                    MARKETS
                                </h1>
                            </div>
                        </div>
                    </div>
                    <div className="relative">
                        <div className="flex items-center">
                            <img src={Slide4} alt="" className="w-screen h-auto"></img>
                            <div className="flex justify-center container absolute">
                                <h1 className="flex headertitle lg:text-6xl md:text-5xl text-2xl lg:max-w-3xl md:max-w-2xl max-w-xs">
                                    WE SUPPLY OUR GAMES
                                    <br />
                                    AND BUILD YOUR GAMES,
                                    <br />
                                    IN ANY MARKET
                                </h1>
                            </div>
                        </div>
                    </div>
                </Slider>
            </div>

            {/* CONTENT */}
            <div className="flex flex-col my-10 md:my-14 lg:my-24 mx-14 2xl:pl-52 xl:pl-20 gap-5 md:gap-7 md:container md:mx-auto">
                <div className="flex lg:justify-start justify-center">
                    <h1 className="maintitle lg:text-7xl md:text-7xl text-4xl lg:max-w-3xl md:max-w-2xl max-w-xs">
                        ABOUT US
                    </h1>
                </div>
                <div className="flex lg:justify-start justify-center container">
                    <p className="maintitle-dsc text-center lg:text-left lg:text-3xl md:text-3xl text-sm lg:max-w-3xl md:max-w-2xl max-w-full" >
                        AURA E GAMING IS A RAPIDLY GROWING
                        INTERNATIONAL COMPANY.HEADQUARTERED
                        IN WILLEMSTAD, CURACAO.OFFERING LOCAL
                        GAMES BUILT FOR LOCAL MARKETS
                    </p>
                </div>
            </div>
            <div className="flex flex-col my-10 md:my-14 lg:my-24 mx-14 2xl:pl-52 xl:pl-20 gap-5 md:gap-7 md:container md:mx-auto">
                <div className="flex lg:justify-start justify-center container">
                    <h1 className="maintitle lg:text-7xl md:text-7xl text-4xl lg:max-w-3xl md:max-w-2xl max-w-xs">
                        OUR GAMES
                    </h1>
                </div>
                <div className="flex lg:justify-start justify-center container">
                    <p className="maintitle-dsc text-center lg:text-left lg:text-3xl md:text-3xl text-sm lg:max-w-3xl md:max-w-2xl max-w-full" >
                        OUR GAMES ARE BUILT TO ENGAGE AND
                        <br />
                        DELIGHT CUSTOMERS
                    </p>
                </div>
            </div>

            {/* DEALERS */}
            <div className="md:container md:mx-auto flex flex-col mt-10 md:my-20 lg:my-14 gap-y-5 md:gap-y-10">
                <div className="flex lg:justify-start justify-center container">
                    <h1 className="maintitle md:text-center lg:text-left md:mx-0 2xl:pl-52 xl:pl-20 lg:text-7xl md:text-6xl text-4xl lg:max-w-6xl md:max-w-lg max-w-sm">
                        LIVE DEALER GAMES
                    </h1>
                </div>

                <div className="flex flex-col md:items-center lg:pl-0 lg:pr-10 md:container md:mx-auto">
                    <div className="flex flex-col lg:flex-row items-center lg:items-start gap-y-11 lg:gap-x-6 pt-5">
                        <div className="md:w-full">
                            <img src={Dealer1} alt=""></img>
                            <h1 className="title lg:text-4xl md:text-5xl text-4xl lg:max-w-3xl max-w-xs mt-7">
                                32 CARDS
                                CASINO
                            </h1>
                            <p className="text-left pt-3 max-w-xs text-base discription" >
                                Brief description of what the game
                                is here, enough to get attention
                            </p>
                            <h1 className="text-left text-lg pt-3 demo">
                                DEMO
                            </h1>
                        </div>
                        <div className="md:w-full">
                            <img src={Dealer2} alt=""></img>
                            <h1 className="title lg:text-4xl md:text-5xl text-4xl lg:max-w-3xl max-w-xs mt-7">
                                HIGH LOW
                            </h1>
                            <p className="text-left pt-3 max-w-xs text-base discription" >
                                Brief description of what the game
                                is here, enough to get
                                attention
                            </p>
                            <h1 className="text-left text-lg pt-3 demo">
                                DEMO
                            </h1>
                        </div>
                        <div className="md:w-full">
                            <img src={Dealer3} alt=""></img>
                            <h1 className="title lg:text-4xl md:text-5xl text-4xl lg:max-w-3xl max-w-xs mt-7">
                                6 PLAY
                                POKER
                            </h1>
                            <p className="text-left pt-3 max-w-xs text-base discription" >
                                Brief description of what the game
                                is here, enough to get
                                <br />
                                attention
                            </p>
                            <h1 className="text-left text-lg pt-3 demo">
                                DEMO
                            </h1>
                        </div>
                    </div>

                    <div className="flex flex-col lg:flex-row items-center lg:items-start gap-y-11 lg:gap-x-6 pt-5">
                        <div className="md:w-full">
                            <img src={Dealer4} alt=""></img>
                            <h1 className="title lg:text-4xl md:text-5xl text-4xl lg:max-w-3xl max-w-xs mt-7">
                                32 CARDS
                                CASINO
                            </h1>
                            <p className="text-left pt-3 max-w-xs text-base discription" >
                                Brief description of what the game
                                is here, enough to get attention
                            </p>
                            <h1 className="text-left text-lg pt-3 demo">
                                DEMO
                            </h1>
                        </div>
                        <div className="md:w-full">
                            <img src={Dealer5} alt=""></img>
                            <h1 className="title lg:text-4xl md:text-5xl text-4xl lg:max-w-3xl max-w-xs mt-7">
                                HIGH LOW
                            </h1>
                            <p className="text-left pt-3 max-w-xs text-base discription" >
                                Brief description of what the game
                                is here, enough to get
                                attention
                            </p>
                            <h1 className="text-left text-lg pt-3 demo">
                                DEMO
                            </h1>
                        </div>
                        <div className="md:w-full">
                            <img src={Dealer6} alt=""></img>
                            <h1 className="title lg:text-4xl md:text-5xl text-4xl lg:max-w-3xl max-w-xs mt-7">
                                6 PLAY
                                POKER
                            </h1>
                            <p className="text-left pt-3 max-w-xs text-base discription" >
                                Brief description of what the game
                                is here, enough to get
                                <br />
                                attention
                            </p>
                            <h1 className="text-left text-lg pt-3 demo">
                                DEMO
                            </h1>
                        </div>
                    </div>

                    <div className="flex flex-col lg:flex-row items-center lg:items-start gap-y-11 lg:gap-x-6 pt-5">
                        <div className="md:w-full">
                            <img src={Dealer7} alt=""></img>
                            <h1 className="title lg:text-4xl md:text-5xl text-4xl lg:max-w-3xl max-w-xs mt-7">
                                32 CARDS
                                CASINO
                            </h1>
                            <p className="text-left pt-3 max-w-xs text-base discription" >
                                Brief description of what the game
                                is here, enough to get attention
                            </p>
                            <h1 className="text-left text-lg pt-3 demo">
                                DEMO
                            </h1>
                        </div>
                        <div className="md:w-full">
                            <img src={Dealer8} alt=""></img>
                            <h1 className="title lg:text-4xl md:text-5xl text-4xl lg:max-w-3xl max-w-xs mt-7">
                                HIGH LOW
                            </h1>
                            <p className="text-left pt-3 max-w-xs text-base discription" >
                                Brief description of what the game
                                is here, enough to get
                                attention
                            </p>
                            <h1 className="text-left text-lg pt-3 demo">
                                DEMO
                            </h1>
                        </div>
                        <div className="md:w-full">
                            <img src={Dealer9} alt=""></img>
                            <h1 className="title lg:text-4xl md:text-5xl text-4xl lg:max-w-3xl max-w-xs mt-7">
                                6 PLAY
                                POKER
                            </h1>
                            <p className="text-left pt-3 max-w-xs text-base discription" >
                                Brief description of what the game
                                is here, enough to get
                                <br />
                                attention
                            </p>
                            <h1 className="text-left text-lg pt-3 demo">
                                DEMO
                            </h1>
                        </div>
                    </div>

                    <div className="flex flex-col lg:flex-row items-center lg:items-start gap-y-11 lg:gap-x-6 pt-5">
                        <div className="md:w-full">
                            <img src={Dealer1} alt=""></img>
                            <h1 className="title lg:text-4xl md:text-5xl text-4xl lg:max-w-3xl max-w-xs mt-7">
                                32 CARDS
                                CASINO
                            </h1>
                            <p className="text-left pt-3 max-w-xs text-base discription" >
                                Brief description of what the game
                                is here, enough to get attention
                            </p>
                            <h1 className="text-left text-lg pt-3 demo">
                                DEMO
                            </h1>
                        </div>
                        <div className="md:w-full">
                            <img src={Dealer2} alt=""></img>
                            <h1 className="title lg:text-4xl md:text-5xl text-4xl lg:max-w-3xl max-w-xs mt-7">
                                HIGH LOW
                            </h1>
                            <p className="text-left pt-3 max-w-xs text-base discription" >
                                Brief description of what the game
                                is here, enough to get
                                attention
                            </p>
                            <h1 className="text-left text-lg pt-3 demo">
                                DEMO
                            </h1>
                        </div>
                        <div className="md:w-full">
                            <img src={Dealer3} alt=""></img>
                            <h1 className="title lg:text-4xl md:text-5xl text-4xl lg:max-w-3xl max-w-xs mt-7">
                                6 PLAY
                                POKER
                            </h1>
                            <p className="text-left pt-3 max-w-xs text-base discription" >
                                Brief description of what the game
                                is here, enough to get
                                <br />
                                attention
                            </p>
                            <h1 className="text-left text-lg pt-3 demo">
                                DEMO
                            </h1>
                        </div>
                    </div>

                    <div className="flex flex-col lg:flex-row items-center lg:items-start gap-y-11 lg:gap-x-6 pt-5">
                        <div className="md:w-full">
                            <img src={Dealer4} alt=""></img>
                            <h1 className="title lg:text-4xl md:text-5xl text-4xl lg:max-w-3xl max-w-xs mt-7">
                                32 CARDS
                                CASINO
                            </h1>
                            <p className="text-left pt-3 max-w-xs text-base discription" >
                                Brief description of what the game
                                is here, enough to get attention
                            </p>
                            <h1 className="text-left text-lg pt-3 demo">
                                DEMO
                            </h1>
                        </div>
                        <div className="md:w-full">
                            <img src={Dealer5} alt=""></img>
                            <h1 className="title lg:text-4xl md:text-5xl text-4xl lg:max-w-3xl max-w-xs mt-7">
                                HIGH LOW
                            </h1>
                            <p className="text-left pt-3 max-w-xs text-base discription" >
                                Brief description of what the game
                                is here, enough to get
                                attention
                            </p>
                            <h1 className="text-left text-lg pt-3 demo">
                                DEMO
                            </h1>
                        </div>
                        <div className="md:w-full">
                            <img src={Dealer6} alt=""></img>
                            <h1 className="title lg:text-4xl md:text-5xl text-4xl lg:max-w-3xl max-w-xs mt-7">
                                6 PLAY
                                POKER
                            </h1>
                            <p className="text-left pt-3 max-w-xs text-base discription" >
                                Brief description of what the game
                                is here, enough to get
                                <br />
                                attention
                            </p>
                            <h1 className="text-left text-lg pt-3 demo">
                                DEMO
                            </h1>
                        </div>
                    </div>

                    <div className="flex flex-col lg:flex-row items-center lg:items-start gap-y-11 lg:gap-x-6 pt-5">
                        <div className="md:w-full">
                            <img src={Dealer10} alt=""></img>
                            <h1 className="title lg:text-4xl md:text-5xl text-4xl lg:max-w-3xl max-w-xs mt-7">
                                32 CARDS
                                CASINO
                            </h1>
                            <p className="text-left pt-3 max-w-xs text-base discription" >
                                Brief description of what the game
                                is here, enough to get attention
                            </p>
                            <h1 className="text-left text-lg pt-3 demo">
                                DEMO
                            </h1>
                        </div>
                        <div className="md:w-full">
                            <img src={Dealer11} alt=""></img>
                            <h1 className="title lg:text-4xl md:text-5xl text-4xl lg:max-w-3xl max-w-xs mt-7">
                                HIGH LOW
                            </h1>
                            <p className="text-left pt-3 max-w-xs text-base discription" >
                                Brief description of what the game
                                is here, enough to get
                                attention
                            </p>
                            <h1 className="text-left text-lg pt-3 demo">
                                DEMO
                            </h1>
                        </div>
                        <div className="md:w-full">
                            <img src={Dealer12} alt=""></img>
                            <h1 className="title lg:text-4xl md:text-5xl text-4xl lg:max-w-3xl max-w-xs mt-7">
                                6 PLAY
                                POKER
                            </h1>
                            <p className="text-left pt-3 max-w-xs text-base discription" >
                                Brief description of what the game
                                is here, enough to get
                                <br />
                                attention
                            </p>
                            <h1 className="text-left text-lg pt-3 demo">
                                DEMO
                            </h1>
                        </div>
                    </div>

                    <div className="flex flex-col lg:flex-row items-center lg:pr-20 lg:mr-72 gap-y-11 lg:gap-x-6 pt-5">
                        <div className="md:w-full">
                            <img src={Dealer13} alt=""></img>
                            <h1 className="title lg:text-4xl md:text-5xl text-4xl lg:max-w-3xl max-w-xs mt-7">
                                32 CARDS
                                CASINO
                            </h1>
                            <p className="text-left pt-3 max-w-xs text-base discription" >
                                Brief description of what the game
                                is here, enough to get attention
                            </p>
                            <h1 className="text-left text-lg pt-3 demo">
                                DEMO
                            </h1>
                        </div>
                        <div className="md:w-full">
                            <img src={Dealer14} alt=""></img>
                            <h1 className="title lg:text-4xl md:text-5xl text-4xl lg:max-w-3xl max-w-xs mt-7">
                                HIGH LOW
                            </h1>
                            <p className="text-left pt-3 max-w-xs text-base discription" >
                                Brief description of what the game
                                is here, enough to get
                                attention
                            </p>
                            <h1 className="text-left text-lg pt-3 demo">
                                DEMO
                            </h1>
                        </div>
                    </div>
                </div>

            </div>
            <div className="md:container md:mx-auto flex flex-col mt-16 md:my-20 lg:my-20 gap-y-5 md:gap-y-10">
                <div className="flex lg:justify-start justify-center container">
                    <h1 className="maintitle md:text-center lg:text-left md:mx-0 2xl:pl-52 xl:pl-20 lg:text-7xl md:text-6xl text-4xl lg:max-w-6xl md:max-w-lg max-w-sm">
                        VIRTUAL GAMES
                    </h1>
                </div>

                <div className="flex flex-col md:items-center lg:pl-0 lg:pr-10 md:container md:mx-auto">
                    <div className="flex flex-col lg:flex-row items-center lg:items-start gap-y-11 lg:gap-x-6 pt-5">
                        <div className="md:w-full">
                            <img src={Dealer1} alt=""></img>
                            <h1 className="title lg:text-4xl md:text-5xl text-4xl lg:max-w-3xl max-w-xs mt-7">
                                32 CARDS
                                CASINO
                            </h1>
                            <p className="text-left pt-3 max-w-xs text-base discription" >
                                Brief description of what the game
                                is here, enough to get attention
                            </p>
                            <h1 className="text-left text-lg pt-3 demo">
                                DEMO
                            </h1>
                        </div>
                        <div className="md:w-full">
                            <img src={Dealer2} alt=""></img>
                            <h1 className="title lg:text-4xl md:text-5xl text-4xl lg:max-w-3xl max-w-xs mt-7">
                                HIGH LOW
                            </h1>
                            <p className="text-left pt-3 max-w-xs text-base discription" >
                                Brief description of what the game
                                is here, enough to get
                                attention
                            </p>
                            <h1 className="text-left text-lg pt-3 demo">
                                DEMO
                            </h1>
                        </div>
                        <div className="md:w-full">
                            <img src={Dealer3} alt=""></img>
                            <h1 className="title lg:text-4xl md:text-5xl text-4xl lg:max-w-3xl max-w-xs mt-7">
                                6 PLAY
                                POKER
                            </h1>
                            <p className="text-left pt-3 max-w-xs text-base discription" >
                                Brief description of what the game
                                is here, enough to get
                                <br />
                                attention
                            </p>
                            <h1 className="text-left text-lg pt-3 demo">
                                DEMO
                            </h1>
                        </div>
                    </div>

                    <div className="flex flex-col lg:flex-row items-center lg:items-start gap-y-11 lg:gap-x-6 pt-5">
                        <div className="md:w-full">
                            <img src={Dealer4} alt=""></img>
                            <h1 className="title lg:text-4xl md:text-5xl text-4xl lg:max-w-3xl max-w-xs mt-7">
                                32 CARDS
                                CASINO
                            </h1>
                            <p className="text-left pt-3 max-w-xs text-base discription" >
                                Brief description of what the game
                                is here, enough to get attention
                            </p>
                            <h1 className="text-left text-lg pt-3 demo">
                                DEMO
                            </h1>
                        </div>
                        <div className="md:w-full">
                            <img src={Dealer5} alt=""></img>
                            <h1 className="title lg:text-4xl md:text-5xl text-4xl lg:max-w-3xl max-w-xs mt-7">
                                HIGH LOW
                            </h1>
                            <p className="text-left pt-3 max-w-xs text-base discription" >
                                Brief description of what the game
                                is here, enough to get
                                attention
                            </p>
                            <h1 className="text-left text-lg pt-3 demo">
                                DEMO
                            </h1>
                        </div>
                        <div className="md:w-full">
                            <img src={Dealer6} alt=""></img>
                            <h1 className="title lg:text-4xl md:text-5xl text-4xl lg:max-w-3xl max-w-xs mt-7">
                                6 PLAY
                                POKER
                            </h1>
                            <p className="text-left pt-3 max-w-xs text-base discription" >
                                Brief description of what the game
                                is here, enough to get
                                <br />
                                attention
                            </p>
                            <h1 className="text-left text-lg pt-3 demo">
                                DEMO
                            </h1>
                        </div>
                    </div>

                    <div className="flex flex-col lg:flex-row items-center lg:items-start gap-y-11 lg:gap-x-6 pt-5">
                        <div className="md:w-full">
                            <img src={Dealer7} alt=""></img>
                            <h1 className="title lg:text-4xl md:text-5xl text-4xl lg:max-w-3xl max-w-xs mt-7">
                                32 CARDS
                                CASINO
                            </h1>
                            <p className="text-left pt-3 max-w-xs text-base discription" >
                                Brief description of what the game
                                is here, enough to get attention
                            </p>
                            <h1 className="text-left text-lg pt-3 demo">
                                DEMO
                            </h1>
                        </div>
                        <div className="md:w-full">
                            <img src={Dealer8} alt=""></img>
                            <h1 className="title lg:text-4xl md:text-5xl text-4xl lg:max-w-3xl max-w-xs mt-7">
                                HIGH LOW
                            </h1>
                            <p className="text-left pt-3 max-w-xs text-base discription" >
                                Brief description of what the game
                                is here, enough to get
                                attention
                            </p>
                            <h1 className="text-left text-lg pt-3 demo">
                                DEMO
                            </h1>
                        </div>
                        <div className="md:w-full">
                            <img src={Dealer9} alt=""></img>
                            <h1 className="title lg:text-4xl md:text-5xl text-4xl lg:max-w-3xl max-w-xs mt-7">
                                6 PLAY
                                POKER
                            </h1>
                            <p className="text-left pt-3 max-w-xs text-base discription" >
                                Brief description of what the game
                                is here, enough to get
                                <br />
                                attention
                            </p>
                            <h1 className="text-left text-lg pt-3 demo">
                                DEMO
                            </h1>
                        </div>
                    </div>

                    <div className="flex flex-col lg:flex-row items-center lg:items-start gap-y-11 lg:gap-x-6 pt-5">
                        <div className="md:w-full">
                            <img src={Dealer1} alt=""></img>
                            <h1 className="title lg:text-4xl md:text-5xl text-4xl lg:max-w-3xl max-w-xs mt-7">
                                32 CARDS
                                CASINO
                            </h1>
                            <p className="text-left pt-3 max-w-xs text-base discription" >
                                Brief description of what the game
                                is here, enough to get attention
                            </p>
                            <h1 className="text-left text-lg pt-3 demo">
                                DEMO
                            </h1>
                        </div>
                        <div className="md:w-full">
                            <img src={Dealer2} alt=""></img>
                            <h1 className="title lg:text-4xl md:text-5xl text-4xl lg:max-w-3xl max-w-xs mt-7">
                                HIGH LOW
                            </h1>
                            <p className="text-left pt-3 max-w-xs text-base discription" >
                                Brief description of what the game
                                is here, enough to get
                                attention
                            </p>
                            <h1 className="text-left text-lg pt-3 demo">
                                DEMO
                            </h1>
                        </div>
                        <div className="md:w-full">
                            <img src={Dealer3} alt=""></img>
                            <h1 className="title lg:text-4xl md:text-5xl text-4xl lg:max-w-3xl max-w-xs mt-7">
                                6 PLAY
                                POKER
                            </h1>
                            <p className="text-left pt-3 max-w-xs text-base discription" >
                                Brief description of what the game
                                is here, enough to get
                                <br />
                                attention
                            </p>
                            <h1 className="text-left text-lg pt-3 demo">
                                DEMO
                            </h1>
                        </div>
                    </div>

                    <div className="flex flex-col lg:flex-row items-center lg:items-start gap-y-11 lg:gap-x-6 pt-5">
                        <div className="md:w-full">
                            <img src={Dealer4} alt=""></img>
                            <h1 className="title lg:text-4xl md:text-5xl text-4xl lg:max-w-3xl max-w-xs mt-7">
                                32 CARDS
                                CASINO
                            </h1>
                            <p className="text-left pt-3 max-w-xs text-base discription" >
                                Brief description of what the game
                                is here, enough to get attention
                            </p>
                            <h1 className="text-left text-lg pt-3 demo">
                                DEMO
                            </h1>
                        </div>
                        <div className="md:w-full">
                            <img src={Dealer5} alt=""></img>
                            <h1 className="title lg:text-4xl md:text-5xl text-4xl lg:max-w-3xl max-w-xs mt-7">
                                HIGH LOW
                            </h1>
                            <p className="text-left pt-3 max-w-xs text-base discription" >
                                Brief description of what the game
                                is here, enough to get
                                attention
                            </p>
                            <h1 className="text-left text-lg pt-3 demo">
                                DEMO
                            </h1>
                        </div>
                        <div className="md:w-full">
                            <img src={Dealer6} alt=""></img>
                            <h1 className="title lg:text-4xl md:text-5xl text-4xl lg:max-w-3xl max-w-xs mt-7">
                                6 PLAY
                                POKER
                            </h1>
                            <p className="text-left pt-3 max-w-xs text-base discription" >
                                Brief description of what the game
                                is here, enough to get
                                <br />
                                attention
                            </p>
                            <h1 className="text-left text-lg pt-3 demo">
                                DEMO
                            </h1>
                        </div>
                    </div>

                    <div className="flex flex-col lg:flex-row items-center lg:items-start gap-y-11 lg:gap-x-6 pt-5">
                        <div className="md:w-full">
                            <img src={Dealer10} alt=""></img>
                            <h1 className="title lg:text-4xl md:text-5xl text-4xl lg:max-w-3xl max-w-xs mt-7">
                                32 CARDS
                                CASINO
                            </h1>
                            <p className="text-left pt-3 max-w-xs text-base discription" >
                                Brief description of what the game
                                is here, enough to get attention
                            </p>
                            <h1 className="text-left text-lg pt-3 demo">
                                DEMO
                            </h1>
                        </div>
                        <div className="md:w-full">
                            <img src={Dealer11} alt=""></img>
                            <h1 className="title lg:text-4xl md:text-5xl text-4xl lg:max-w-3xl max-w-xs mt-7">
                                HIGH LOW
                            </h1>
                            <p className="text-left pt-3 max-w-xs text-base discription" >
                                Brief description of what the game
                                is here, enough to get
                                attention
                            </p>
                            <h1 className="text-left text-lg pt-3 demo">
                                DEMO
                            </h1>
                        </div>
                        <div className="md:w-full">
                            <img src={Dealer12} alt=""></img>
                            <h1 className="title lg:text-4xl md:text-5xl text-4xl lg:max-w-3xl max-w-xs mt-7">
                                6 PLAY
                                POKER
                            </h1>
                            <p className="text-left pt-3 max-w-xs text-base discription" >
                                Brief description of what the game
                                is here, enough to get
                                <br />
                                attention
                            </p>
                            <h1 className="text-left text-lg pt-3 demo">
                                DEMO
                            </h1>
                        </div>
                    </div>

                    <div className="flex flex-col lg:flex-row items-center lg:pr-20 lg:mr-72 gap-y-11 lg:gap-x-6 pt-5">
                        <div className="md:w-full">
                            <img src={Dealer13} alt=""></img>
                            <h1 className="title lg:text-4xl md:text-5xl text-4xl lg:max-w-3xl max-w-xs mt-7">
                                32 CARDS
                                CASINO
                            </h1>
                            <p className="text-left pt-3 max-w-xs text-base discription" >
                                Brief description of what the game
                                is here, enough to get attention
                            </p>
                            <h1 className="text-left text-lg pt-3 demo">
                                DEMO
                            </h1>
                        </div>
                        <div className="md:w-full">
                            <img src={Dealer14} alt=""></img>
                            <h1 className="title lg:text-4xl md:text-5xl text-4xl lg:max-w-3xl max-w-xs mt-7">
                                HIGH LOW
                            </h1>
                            <p className="text-left pt-3 max-w-xs text-base discription" >
                                Brief description of what the game
                                is here, enough to get
                                attention
                            </p>
                            <h1 className="text-left text-lg pt-3 demo">
                                DEMO
                            </h1>
                        </div>
                    </div>
                </div>

            </div>


            {/* FOOTER */}
            <div className="flex flex-col px-4 lg:px-0 mt-10 bg-gray-200 pb-20">
                <div className="flex pt-10 md:pt-24 justify-center lg:justify-start  md:container md:mx-auto">
                    <h1 className="maintitle xl:text-8xl lg:text-7xl md:text-7xl text-4xl lg:max-w-3xl md:max-w-2xl max-w-full lg:pl-40">
                        CONTACT US
                    </h1>
                </div>
                <div className="flex flex-col relative items-center gap-10 md:gap-12 lg:gap-0 lg:flex-row pt-10 md:pt-20 lg:justify-start md:container md:mx-auto ">
                    <div className="flex">
                        <h1 className="title lg:pl-44 2xl:text-5xl xl:text-4xl lg:text-3xl md:text-3xl text-xl lg:max-w-4xl md:max-w-none max-w-xl mt-0" >
                            E: HELLO@AURAEGAMING.COM
                        </h1>
                    </div>
                    <div className=" flex lg:absolute lg:inset-y-auto lg:right-20">
                        <h1 className="title 2xl:text-5xl xl:text-4xl lg:text-3xl md:text-3xl text-xl lg:max-w-3xl md:max-w-none max-w-xl mt-0 " >
                            T: 012345678910
                        </h1>
                    </div>
                </div>
                <div className="flex flex-col gap-10 lg:flex-row lg:pl-40 lg:justify-start 2xl:gap-96 xl:gap-64 lg:gap-32 items-center gap-x-6 pt-10 md:pt-20 text-4xl lg:text-3xl  md:container md:mx-auto">
                    <h1 className="text-center lg:text-left footer font-medium" >
                        CERTIFICATION
                        <br />
                        LOGO HERE
                    </h1>
                    <h1 className="text-center lg:text-left footer font-medium" >
                        LICENSING
                        <br />
                        LOGO HERE
                    </h1>
                    <h1 className="text-center lg:text-left footer lg:right-40 font-medium" >
                        LICENSING
                        <br />
                        LOGO HERE
                    </h1>
                </div>
            </div>
            <div className="footerbg">
                <div className="relative flex flex-col lg:flex-row lg:px-40  pt-10 lg:pt-20 pb-10 lg:pb-36 gap-16 md:text-2xl lg:justify-start lg:gap-0 md:container md:mx-auto">
                    <div className="footer flex flex-col items-center lg:items-start md:container md:mx-auto" >
                        <img src={Logo} alt="" className="lg:w-1/2 lg:max-w-max lg:h-auto"></img>
                        <h1 className="text-center footer pt-10 text-base lg:text-sm xl:text-base">
                            AURA SOFTWARE LIMITED
                        </h1>
                    </div>
                    <div className="flex flex-col items-center lg:items-start lg:text-lg xl:text-2xl md:container md:mx-auto">
                        <h1 className="footer2">
                            OUR GAMES
                        </h1>
                        <h1 className="footer2 pt-9">
                            CONTACT US
                        </h1>
                    </div>
                    <div className="flex flex-col items-center lg:items-start lg:text-lg xl:text-2xl md:container md:mx-auto">
                        <h1 className="footer2">
                            PRIVACY POLICY
                        </h1>
                        <h1 className="footer2 pt-9">
                            COOKIES POLICY
                        </h1>
                        <h1 className="footer2 pt-9">
                            LEAGAL DISCLAIMER
                        </h1>
                    </div>
                    <div className="footer2 flex flex-col items-center lg:items-end lg:text-lg xl:text-2xl md:container md:mx-auto lg:absolute lg:inset-y-auto lg:right-16">
                        <img src={Back} alt="" className=""></img>
                        <h1 className="footer pt-3">
                            BACK TO TOP
                        </h1>
                    </div>
                </div>
            </div>
        </div>
    )

}

export default Landing

